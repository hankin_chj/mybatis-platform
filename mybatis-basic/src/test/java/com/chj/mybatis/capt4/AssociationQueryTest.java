package com.chj.mybatis.capt4;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.chj.mybatis.entity.TRole;
import com.chj.mybatis.mapper.TRoleMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import com.chj.mybatis.entity.TUser;
import com.chj.mybatis.mapper.TUserMapper;
import com.chj.mybatis.mapper.TUserMapperExt;

/**
 * @author Administrator
 */
public class AssociationQueryTest {

    private SqlSessionFactory sqlSessionFactory;

    @Before
    public void init() throws IOException {

        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        // 1.读取mybatis配置文件创SqlSessionFactory
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        inputStream.close();
    }


    @Test
    // 1 to 1两种关联方式
    public void testOneToOne() {
        // 2.获取sqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // 3.获取对应mapper
        TUserMapperExt mapper = sqlSession.getMapper(TUserMapperExt.class);
        // 4.执行查询语句并返回结果集
        // ----------------------
        List<TUser> list1 = mapper.selectUserPosition1();
        for (TUser tUser : list1) {
            System.out.println(tUser);
        }

        System.out.println("===================================================");

        List<TUser> list2 = mapper.selectUserPosition2();
        for (TUser tUser : list2) {
            System.out.println(tUser);
        }
    }


    @Test
    // 1对多 两种关联方式
    public void testOneToMany() {
        // 2.获取sqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // 3.获取对应mapper
        TUserMapperExt mapper = sqlSession.getMapper(TUserMapperExt.class);
        // 4.执行查询语句并返回结果
        // ----------------------
        List<TUser> selectUserJobs1 = mapper.selectUserJobs1();
        List<TUser> selectUserJobs2 = mapper.selectUserJobs2();
        for (TUser tUser : selectUserJobs1) {
            System.out.println(tUser);
        }
        System.out.println("===============================================================");
        for (TUser tUser : selectUserJobs2) {
            System.out.println(tUser.getJobs().size());
        }
    }

    /**
     * discriminator   鉴别器映射
     * 鉴别器非常容易理解,因为它的表现很像 Java 语言中的 switch 语句；
     * <discriminator column="sex"  javaType="int">
     * <case value="1" resultMap="userAndHealthReportMale"/>
     * <case value="2" resultMap="userAndHealthReportFemale"/>
     * </discriminator>
     */
    @Test
    public void testDiscriminator() {
        // 2.获取sqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // 3.获取对应mapper
        TUserMapperExt mapper = sqlSession.getMapper(TUserMapperExt.class);
        // 4.执行查询语句并返回结果
        // ----------------------
        List<TUser> list = mapper.selectUserHealthReport();
        for (TUser tUser : list) {
            System.out.println(tUser);
        }
    }

    /**
     * 多对多
     */
//	@Test
//	public void testManyToMany() {
//		// 2. 获取sqlSession
//		SqlSession sqlSession = sqlSessionFactory.openSession();
//		// 3. 获取对应mapper
//		TUserMapperExt mapper = sqlSession.getMapper(TUserMapperExt.class);
//		// 4. 执行查询语句并返回结果
//		// ----------------------
//		// 嵌套结果
//		List<TUser> list = mapper.selectUserRole();
//		for (TUser tUser : list) {
//			System.out.println(tUser.getRoles());
//		}
//		// 嵌套查询
//		TRoleMapper roleMapper = sqlSession.getMapper(TRoleMapper.class);
//		List<TRole> roles = roleMapper.selectRoleandUsers();
//		System.out.println("================主表查询结束=====================");
//		for (TRole tRole : roles) {
//			System.out.println(tRole.getUsers());
//		}
//	}


    // 多对多 嵌套
    @Test
    public void testManyToMany() {
        // 2.获取sqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // 3.获取对应mapper
        TUserMapperExt mapper = sqlSession.getMapper(TUserMapperExt.class);
        // 4.执行查询语句并返回结果
        // ----------------------
        //嵌套结果
        List<TUser> list = mapper.selectUserRole();
        for (TUser tUser : list) {
            System.out.println("tUser.getRoles().size==" + tUser.getRoles().size());
        }
        // 嵌套查询
        TRoleMapper roleMapper = sqlSession.getMapper(TRoleMapper.class);
        List<TRole> roles = roleMapper.selectRoleandUsers();
        System.out.println("================主表查询结束=====================");
        for (TRole tRole : roles) {
            System.out.println("tRole.getUsers()==" + tRole.getUsers());
        }
    }

}
