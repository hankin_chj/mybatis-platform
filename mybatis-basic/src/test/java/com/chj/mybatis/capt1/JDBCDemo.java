package com.chj.mybatis.capt1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.chj.mybatis.entity.TUser;


public class JDBCDemo {

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/mybatis?useUnicode=true&characterEncoding=utf8&allowMultiQueries=true";

    static final String USER = "root";
    static final String PASS = "root";

    @Test
    public void QueryStatementDemo() {

        Connection conn = null;
        Statement stmt = null;
        List<TUser> users = new ArrayList<TUser>();

        try {
            //step2 regist mysql driver
            Class.forName(JDBC_DRIVER);

            //step3 get a connection
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connecting to database...");

            //step4 create a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String username = "lison";
            String sql = "SELECT * FROM t_user where user_name='" + username + "'";
            // execute query
            ResultSet rs = stmt.executeQuery(sql);
            System.out.println(stmt.toString());

            //STEP 5: 从resultSet中获取数据并转化成bean
            while (rs.next()) {
                TUser user = new TUser();
                user.setId(rs.getInt("id"));
                user.setUserName(rs.getString("user_name"));
                user.setRealName(rs.getString("real_name"));
                user.setSex(rs.getByte("sex"));
                user.setMobile(rs.getString("mobile"));
                user.setEmail(rs.getString("email"));
                user.setNote(rs.getString("note"));
                System.out.println(user.toString());

                users.add(user);
            }
            // step6 close the connection
            rs.close();
            stmt.close();
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        System.out.println("-------------------------");
        System.out.println("there are " + users.size() + " users in the list!");

    }


    @Test
    public void updateDemo() {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            // STEP 2: 注册mysql的驱动
            Class.forName("com.mysql.jdbc.Driver");

            // STEP 3: 获得一个连接
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // STEP 4: 启动手动提交
            conn.setAutoCommit(false);

            // STEP 5: 创建一个更新
            System.out.println("Creating statement...");
            String sql = "update t_user  set mobile= ? where user_name= ? ";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, "186995587411");
            stmt.setString(2, "lison");
            System.out.println(stmt.toString());//打印sql
            int ret = stmt.executeUpdate();
            System.out.println("此次修改影响数据库的行数为：" + ret);

            // STEP 6: 手动提交数据
            conn.commit();

            // STEP 7: 关闭连接
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // Handle errors for JDBC
            try {
                conn.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            se.printStackTrace();
        } catch (Exception e) {
            try {
                conn.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            // finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }


}
