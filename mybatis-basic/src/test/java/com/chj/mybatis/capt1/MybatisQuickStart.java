package com.chj.mybatis.capt1;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import com.chj.mybatis.entity.TUser;
import com.chj.mybatis.mapper.TUserMapper;

public class MybatisQuickStart {

    private SqlSessionFactory sqlSessionFactory;

    /**
     * init SqlSessionFactory
     *
     * @throws IOException
     */
    @Before
    public void init() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        // 1. read mybatis config file and create sqlSessionFactory
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        inputStream.close();

    }

    @Test
    public void quickStart() {
        // 2. achieve sqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // 3. achieve mapper
        TUserMapper mapper = sqlSession.getMapper(TUserMapper.class);
        // 4. execute query and return result
        TUser user = mapper.selectByPrimaryKey(1);
        System.out.println(user.toString());
    }

    // 快速入门本质分析 ibatis方式
    @Test
    public void quickStartOriginal() {
        // 2.获取sqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // 3.执行查询语句并返回单条数据
        TUser user = sqlSession.selectOne("com.chj.mybatis.mapper.TUserMapper.selectByPrimaryKey", 1);
        System.out.println(user.toString());
    }

}
