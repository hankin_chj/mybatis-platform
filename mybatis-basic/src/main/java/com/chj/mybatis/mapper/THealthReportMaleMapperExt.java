package com.chj.mybatis.mapper;

import java.util.List;

import com.chj.mybatis.entity.THealthReportMale;

public interface THealthReportMaleMapperExt extends THealthReportMaleMapper {

    List<THealthReportMale> selectByUserId(Integer userID);

}