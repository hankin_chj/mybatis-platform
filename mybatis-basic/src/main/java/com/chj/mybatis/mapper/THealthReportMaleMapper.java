package com.chj.mybatis.mapper;

import com.chj.mybatis.entity.THealthReportMale;

public interface THealthReportMaleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(THealthReportMale record);

    int insertSelective(THealthReportMale record);

    THealthReportMale selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(THealthReportMale record);

    int updateByPrimaryKey(THealthReportMale record);
}