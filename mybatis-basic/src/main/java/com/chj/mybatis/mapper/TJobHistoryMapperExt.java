package com.chj.mybatis.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.chj.mybatis.entity.TJobHistory;
import com.chj.mybatis.entity.TUser;

public interface TJobHistoryMapperExt extends TJobHistoryMapper {


    List<TJobHistory> selectByUserId(int userId);

    List<TUser> selectByEmailAndSex2(@Param("email") String email, @Param("sex") Byte sex);
}