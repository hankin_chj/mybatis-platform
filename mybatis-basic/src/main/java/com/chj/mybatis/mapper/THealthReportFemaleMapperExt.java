package com.chj.mybatis.mapper;

import java.util.List;

import com.chj.mybatis.entity.THealthReportFemale;

public interface THealthReportFemaleMapperExt extends THealthReportFemaleMapper {

    List<THealthReportFemale> selectByUserId(Integer userId);

}