package com.chj.mybatis.decorator.decorators;


import com.chj.mybatis.decorator.Girl;

public class SexyGirl implements Girl {
    private Girl girl;

    public SexyGirl(Girl girl) {
        super();
        this.girl = girl;
    }

    public void dance() {
        System.out.println("着装性感……");
        girl.dance();

    }

}
