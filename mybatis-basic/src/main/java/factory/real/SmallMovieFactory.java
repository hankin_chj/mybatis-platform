package factory.real;

import factory.product.SmallMovie;

public interface SmallMovieFactory {

    public SmallMovie createMovie();
}
